var controllers = {};
controllers.SimpleController = function ($scope, SimpleFactory, $location){
	    $scope.sortorder = 'name';
		$scope.customers = [];
		$scope.master= {};
		
		init();
		
		function init(){
			$scope.customers = SimpleFactory.getCustomers();
		}
						  
		$scope.addCustomer = function () {
			var cust = {
				name: $scope.newCustomer.name,
				description: $scope.newCustomer.description
			};
			SimpleFactory.addCustomer(cust);
			$location.url('/customers');
		};		
					  
	  			  
	  
	};
	
controllers.NavbarController = function ($scope, $location) {
		$scope.getClass = function (path) {
			if ($location.path().substr(0, path.length) == path) {
				return true
			} else {
				return false;
			}
		}
	};
	
demoApp.controller (controllers);