demoApp.factory ('SimpleFactory', function ($http){
	
	var customers = [];
	
	$http.get('http://localhost:81/tanveer/angular/demo/get_customers.php').success(function (response) {
		
		angular.forEach(response, function(value, index){
			customers.push(
				{
				  'name' : value.name,
				  'description' : value.description,		
				}
			);
		});
		
		
	}).error(function(err){
	});
	
	var factory = {};
	
	factory.getCustomers = function (){
		  return customers;
	  }
	  
	 factory.addCustomer = function (cust){
		   $http.post('http://localhost:81/tanveer/angular/demo/save_customer.php', cust);
		   customers.push(
				{
				  'name' : cust.name,
				  'description' : cust.description,		
				}
			);
	 }
	  
	return factory;
});