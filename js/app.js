﻿var demoApp = angular.module('demoApp', []);
  
demoApp .config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/customers', {
        templateUrl: 'templates/customers.html',
        controller: 'SimpleController'
      }).
      when('/add-new-customer', {
        templateUrl: 'templates/add_customer.html',
        controller: 'SimpleController'
      }).
      otherwise({
        redirectTo: '/customers'
      });
  }]);