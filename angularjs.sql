-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2014 at 10:47 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `angularjs`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `description`) VALUES
(1, 'Gatner Pakistan Limit', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(2, 'Net Lawman Limit', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(3, 'Nextbridge (Pvt) Limit', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(4, 'Where the World Meet', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(5, 'Red Signal', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(6, 'Nextbridge (Pvt) Multan Center', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(7, 'isla', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(8, 'Nextbridge Islamabad Center', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(9, 'WtWM', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.'),
(10, 'Pure Logic', 'In the series premiere of this medical drama set in a 1900s New York City hospital, accomplished Dr. John W. Thackery is named chief surgeon and is pressured by a major benefactor to hire a black doctor as his top assistant.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
